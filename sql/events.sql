 select 

distinct(fie.event_id),
dhcu2.master_hcu_id,
dvad.vaccine,
dvad.indication,
case indication
            when '1' then 'Over 65 years (Influenza)'
            when '2' then 'Under 16 years, eligible condition (Influenza)'
            when '3' then 'Eligible condition (Influenza)'
            when '4' then 'Sexual or household contact'
            when '5' then 'Primary course'
            when '6' then 'Booster or Extra dose'
            when '7' then 'Post Partum'
            when '8' then 'Low birth weight'
            when '9' then 'HepB carrier mother'
            when '10' then 'At risk for TB'
            when '11' then 'Pre/Post Splenectomy'
            when '12' then 'At risk for Pneumococcal; No previous history'
            when '13' then 'At risk for Pneumococcal; Previous conjugate/PCV7 history'
            when '14' then 'At risk for Pneumococcal; Previous polysaccharide/23PPV history'
            when '16' then 'Pregnant'
            when '6W' then '6 weeks'
            when '3M' then '3 months'
            when '5M' then '5 months'
            when '12M' then '12 months'
            when '15M' then '15 months'
            when '4Y' then '4 years'
            when '11Y' then '11 years'
            when '45Y' then '45 years'
            when '65Y' then '65 years'
            else 'Unknown'
            end as indication_description, 
dves.event_status_description,
dves.event_sub_status_description,
dvd.sql_date vaccination_date,
extract (year from dvd.sql_date) vaccination_year,
fie.responsible_provider_type vaccinator_registration_type,
drc.clinic_name responsible_clinic_name,
drc.PHO_name responsible_clinic_PHO,
drc.DHB_name responsible_clinic_DHB,
dgsl.dhb DHB_of_domicile,
dgsl.dhb_code,
dgsl.domicile_name,
da.priority_ethnic_code,
case when substr(da.priority_ethnic_code, 1,2) = 11 then 'NZ European'
     when substr(da.priority_ethnic_code, 1,1) = 2 then 'Maori'
     when substr(da.priority_ethnic_code, 1,1) = 3 then 'Pacific'
     when substr(da.priority_ethnic_code, 1,1) = 4 then 'Asian'
else 'Other' end as nir_ethnicity,
dhcu2.date_of_birth,
extract (year from dhcu2.date_of_birth) birth_year,
trunc((dvd.sql_date-dhcu2.date_of_birth)/365.25) age_at_vaccination,
trunc(months_between(dvd.sql_date, dhcu2.date_of_birth)) age_in_months




from dssadm_rep.fact_immunisation_events fie

inner join 
dssadm_rep.dim_vaccine_antigen_dose dvad              on dvad.dim_vaccine_antigen_do_key = fie.dim_vaccine_antigen_do_key

inner join
dssadm_rep.dim_vaccination_event_status dves      on dves.dim_vaccination_event__key = fie.dim_vaccination_event__key

inner join
 dssadm_rep.dim_vaccination_date dvd             on dvd.dim_vaccination_date_key = fie.dim_vaccination_date_key

inner join 
 dssadm_rep.dim_hcu_identifiable dhcu            on dhcu.dim_hcu_identifiable_key = fie.dim_health_care_user_key

--this join enables you to take master demographic info only
inner join 
dssadm_rep.dim_hcu_identifiable dhcu2             on dhcu.master_hcu_id = dhcu2.hcu_id  

--Left join as do not want to exclude people with poor geographic info or mismatch between NHI data and NIR enrol data
left join 
dssadm_rep.dim_geo_scd dgsl                        on dgsl.domicile_code = dhcu2.domicile_code   

left join
dssadm_rep.dim_affiliation da                     on da.dim_affiliation_key = dhcu2.dim_affiliation_key

--left join as clinic dimension tables are not always fully populated so can drop rows on an inner join
left join
dssadm_rep.dim_responsible_clinic drc                on drc.dim_responsible_clinic_key = fie.dim_responsible_clinic_key

where 

--Better to add these conditions in R but just put here as a reminder to apply them in R cleaning code.
--dves.event_status_description = 'Completed' and

--dves.event_sub_status_description not in 'Alternative given' and

--dvad.vaccine in ('MMR') and

--Specify a vaccination date range if relevant
dvd.sql_date between to_date('01/07/2021') and to_date('30/06/2022') and  

--Specify a date of birth range if wanting to do by birth cohort
--dhcu2.date_of_birth between '01/01/2010' and '31/12/2018'

--Exclude events on bad or missing NHIs
fie.dim_health_care_user_key > 0;
              